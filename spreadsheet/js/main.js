(function () {

	/**
	* Center the main-window on the viewport
	*/
	$(window).resize(function(){

	    $('.centerwindow').css({
	        position:'absolute',
	        left: ($(window).width() - $('.centerwindow').outerWidth())/2,
	        top: ($(window).height() - $('.centerwindow').outerHeight())/2
	    });

	});
	$(window).resize();

	/**
	* Fill the grid	
	*/
	var fillTable = function(data) {
		
		var colHeaders = [];
		var colFormat = [];
		var colSlug = [];
		var cleanData = [];

		$.each(data.feed.entry, function(key, value) {
			
			var title = value.title.$t;
			var content = value.content.$t;

			var col = title.replace(/[0-9]/g, '');
			var row = title.replace(/[A-Z]/g, '');

			// is a number
			if (!isNaN(parseFloat(content)) && isFinite(content)) {
				content = numeral(content).format('0,0.00');				
			}
			if (row === "2") {
				colFormat.push(numeral().unformat(content) !== 0 ? 'number' : 'text');
			}

			// check if value is on first row
			if (row === "1") {
				colHeaders.push(content);
				colSlug.push(content.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,''));
			}
			else if (col === 'A') {
				cleanData.push([content]);
			}
			else {
				row -= 2;
				cleanData[row].push(content);
			}
		});

		var tbl_header = "";
		var tbl_body = "";

		$.each(colHeaders, function(k, v) {
			var className = k === 1 ? 'first header' : 'header';
			tbl_header += "<th class='"+className+" "+colSlug[k]+" "+colFormat[k]+" col"+k+"'>"
        				+ "<div class='th-inner'>"+v+"<span class='sortArrow'>&nbsp;</span></div>"
						+ "</th>";			             
		});

		$.each(cleanData, function(n, row) {
			var tbl_row = "";
			$.each(row, function(k , v) {				
				var addClass = '';
				if (colFormat[k] === 'number' && v.match(/%$/)) {
					addClass = numeral().unformat(v) >= 0 ? ' colorify-up' : ' colorify-down'; 
				}
				tbl_row += "<td class='"+colSlug[k]+" "+colFormat[k]+" col"+k+""+addClass+"'>"+v+"</td>";
			});
			var zebra = (n % 2 == 0 ? 'even' : 'odd');
			tbl_body += "<tr class='" + zebra + "'>"+tbl_row+"</tr>";                 
		});

		$("#table").html("<thead><tr>"+tbl_header+"</tr></thead><tbody>"+tbl_body+"</tbody>");
		$.each(colHeaders, function(k, v) {
			$("th."+ colSlug[k] +" > div").width($($("table tbody td."+colSlug[k])[0]).width());
		});

	    // add parser through the tablesorter addParser method 
	    $.tablesorter.addParser({ 
	        // set a unique id 
	        id: 'formatednumeric', 
	        is: function(s) { 
	            // return false so this parser is not auto detected 
	            return false; 
	        }, 
	        format: function(s) { 
	            // format your data for normalization 
	            return numeral().unformat(s); 
	        }, 
	        // set type, either numeric or text 
	        type: 'numeric' 
	    }); 
		
		var headers = {};
	    $.each(colFormat, function(k, v) {
	    	if (v === 'number') {
	    		headers[k] = {sorter:'formatednumeric'};
	    	}
	    });

		$("#table").tablesorter({
			sortList:[[0,0]],
			headers: headers,
	        widgets: ['zebra'],
		});

		// highlight plugin
		$("#table").tableHover({
			colClass: 'highlight',
			rowClass: 'highlight'
		});



	}

	/**
    * Fetch data from Google Docs
    */
    var spreadsheetURL = 'https://spreadsheets.google.com/feeds/cells/0Aii0IVxxk1b6dEdxRlczZmxGYU1RTHA0LUhPYXJCaHc/od6/public/basic?alt=json';

    // TODO: ajax activity gif
	$.getJSON(spreadsheetURL)
	.done(fillTable)
	.fail(function() {
		alert('Failed to fetch spreadsheet! Do you have network?');
	});

}());